//
//  LDFSagaGen.h
//  Labb2Frid
//
//  Created by IT-Högskolan on 27/01/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDFSagaGen : NSObject
-(id)init;

-(NSString*)returnSagaWithPeople:(BOOL)people andFriends:(int)friends andEnding:(int)end;

@end
