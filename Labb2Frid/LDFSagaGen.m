//
//  LDFSagaGen.m
//  Labb2Frid
//
//  Created by IT-Högskolan on 27/01/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFSagaGen.h"
@interface LDFSagaGen()
@property (nonatomic) NSString *randName1;
@property (nonatomic) NSString *randName2;
@property (nonatomic) NSString *randName3;
@property (nonatomic) NSString *randName4;
@property (nonatomic) NSString *randNouns1;
@property (nonatomic) NSString *randAdjective1;
@property (nonatomic) NSString *friends;
@property (nonatomic) NSArray *evilnames;
@property (nonatomic) NSArray *names;
@property (nonatomic) NSArray *adjectives;
@property (nonatomic) NSArray *nouns;

@end

@implementation LDFSagaGen

-(id)init{
    self = [super init];
    if(self){
        [self setArrays];
    }
    return self;
}

-(void)setArrays{
    self.names = @[@"Carl", @"Gustav", @"Knut", @"Kristian", @"Sven", @"Magnus", @"Göran" ,@"Dregen", @"Janus"];
    self.evilnames = @[@"Läbbiga Carl", @"Grymma Gustav", @"Äckliga Knut", @"Bösiga Kristian", @"Smutsiga Sven", @"Elaka Magnus", @"Dryga Dregen", @"Jävliga Janus", @"Gris Göran",];
    self.nouns = @[@"moped", @"blodpudding", @"blåbär", @"fotboll", @"tidning", @"flyktbil"];
    self.adjectives = @[@"blöta", @"unkna", @"fräsha", @"väldoftande", @"dammiga", @"gamla"];
}

-(void)setRandomWords:(BOOL) nicePeople{
    if(nicePeople){
        self.randName1 = [self randomWord: self.names];
        self.randName2 = [self randomWord: self.names];
        self.randName3 = [self randomWord: self.names];
        self.randName4 = [self randomWord: self.names];
    } else if(!nicePeople) {
        self.randName1 = [self randomWord: self.evilnames];
        self.randName2 = [self randomWord: self.evilnames];
        self.randName3 = [self randomWord: self.evilnames];
        self.randName4 = [self randomWord: self.evilnames];
    }
    self.randAdjective1 = [self randomWord: self.adjectives];
    self.randNouns1 = [self randomWord: self.nouns];
 }

-(NSString*)randomWord: (NSArray*)array{
    return array[arc4random() % array.count];
}

-(NSString*)returnSagaWithPeople:(BOOL)people andFriends:(int)friends andEnding:(int)end{
    [self setRandomWords:people];
    [self makeFriends:friends andType:people];
    if(end == 1){
        return [self setSagaWithEnd:[self lovelyEnd]];
    } else { return [self setSagaWithEnd:[self horrorEnd]];}
}

-(NSString*)horrorEnd{
     return [NSString stringWithFormat:@"%@en fick till slut så mycket jord över sig att den kvävdes till döds. Som spöke terroriserade den %@ %@en %@ till tids ände", self.randNouns1, self.randAdjective1, self.randNouns1, self.randName1 ];
}
-(NSString*)lovelyEnd{
    return [NSString stringWithFormat: @"Vis av erfarenhet skakade %@en av sig jorden, stampade till den och tog ett steg uppåt. Till slut hade den %@ %@en stampat så mycket jord att den ta sig ur brunnen.", self.randNouns1, self.randAdjective1, self.randNouns1 ];
}

-(void)makeFriends:(int)friend andType:(BOOL)type{
    self.friends = @"";
    if(type){
        for (int i = 0; i < friend; i++) {
            self.friends = [self.friends stringByAppendingString:[self randomWord:self.names]];
            self.friends = [self.friends stringByAppendingString: @", "];
        }
    }
    else {
        for (int i = 0; i < friend; i++) {
            NSLog(@"else: %@", self.friends);
            self.friends = [self.friends stringByAppendingString:[self randomWord:self.evilnames]];
            self.friends = [self.friends stringByAppendingString: @", "];
        }
    }
}
//The whole story with ending.
//Maybe better if cut up in several strings for better overvriew.
-(NSString*)setSagaWithEnd:(NSString*)end{
    return [NSString stringWithFormat: @"En dag föll %@ %@ %@ ned i en brunn. %@en tjöt högt medan %@ försökte fundera hur han skulle göra för att få upp den ur brunnen. Slutligen bestämde han sig för att %@en ju var så gammal och att han ändå hade tänkt att fylla igen brunnen, då kunde han ju lika gärna begrava %@en i brunnen direkt. Det var bara inte värt att försöka få upp den ur brunnen. Han bjöd in sina grannar, %@ för att hjälpa honom med att skyffla jord och grus i brunnen.Den %@ %@en började nu förstå vad som var på gång och tjöt fruktansvärt. %@",self.randName1 , self.randAdjective1, self.randNouns1, self.randNouns1, self.randName1, self.randNouns1, self.randNouns1, self.friends, self.randAdjective1,  self.randNouns1, end];
}
@end
