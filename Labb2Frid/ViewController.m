//
//  ViewController.m
//  Labb2Frid
//
//  Created by IT-Högskolan on 26/01/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"
#import "LDFSagaGen.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *sagaTextView;
@property (weak, nonatomic) IBOutlet UISwitch *endingButton;
@property (weak, nonatomic) IBOutlet UISwitch *peopleSwitch;
@property (weak, nonatomic) IBOutlet UILabel *numberOfNeighbours;
@property (weak, nonatomic) IBOutlet UISlider *neighbourSlider;

@property (nonatomic) int ending;
@property (nonatomic) BOOL peopleType;
@property (nonatomic) int friends;

@end

@implementation ViewController
LDFSagaGen *mySaga;
- (IBAction)neighbourSlide:(id)sender {
    self.neighbourSlider.minimumValue = 1;
    self.neighbourSlider.maximumValue = 5;
    self.numberOfNeighbours.text = [NSString stringWithFormat:@"%.2f",self.neighbourSlider.value];
}


- (IBAction)neighbourSlide:(id)sender {
    self.neighbourSlider.minimumValue =  1;
    self.neighbourSlider.maximumValue = 5;
    self.numberOfNeighbours.text = [NSString stringWithFormat:@"%.2f",self.neighbourSlider.value];
}

- (IBAction)generateSaga:(id)sender {
    self.friends = self.neighbourSlider.value;
   
    if(self.endingButton.on){self.ending = 1;}
    else {self.ending = 0;}
    
    if(self.peopleSwitch.on){self.peopleType = YES;}
    else {self.peopleType = NO;}
    
    self.sagaTextView.text = [mySaga returnSagaWithPeople:self.peopleType andFriends:self.friends andEnding:self.ending];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    mySaga = [[LDFSagaGen alloc] init];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
